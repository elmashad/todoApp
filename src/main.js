import Vue from 'vue'
import App from './App.vue'

require('../node_modules/bootstrap/dist/css/bootstrap.min.css')

global.jQuery = require('jquery')
//global.Tether = require('tether')
global.Popper = require('popper.js')

require('bootstrap');

new Vue({
  el: '#app',
  render: h => h(App)
})
